package com.company.driverFactory;

import com.company.driverFactory.driverObjects.ChromeWebDriverManager;
import com.company.driverFactory.driverObjects.WebDriverManager;
import com.company.driverFactory.driverObjects.FirefoxWebDriverManager;
import com.company.enums.DriverTypeEnum;

public class GetWebDriverFactory {

    public WebDriverManager constructDriverByType(DriverTypeEnum type) {

        if (type == DriverTypeEnum.CHROME)
            return new ChromeWebDriverManager();

        if (type == DriverTypeEnum.FIREFOX)
            return new FirefoxWebDriverManager();

        return null;
    }
}
