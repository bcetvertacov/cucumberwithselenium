package com.company.driverFactory.driverObjects;

import org.openqa.selenium.WebDriver;

public abstract class WebDriverManager {
    protected String pathToDriver;
    protected String driverEntryKey;
    protected WebDriver driver;

    protected abstract void setPathToDriver();

    protected abstract void setDriverEntryKey();

    protected abstract void setDriver();

    protected void setDriverProprety(String driverEntryKey, String pathToDriver) {
        System.setProperty(driverEntryKey, pathToDriver);

    }

    public WebDriver getWebDriver() {
        return driver;
    }
}
