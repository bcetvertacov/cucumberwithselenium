package com.company.driverFactory.driverObjects;

import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxWebDriverManager extends WebDriverManager {

    private final String FIREFOX_DRIVER_PATH = "drivers/geckodriver.exe";
    private final String FIREFOX_PROPERTY_KEY = "webdriver.gecko.driver";

    @Override
    public void setPathToDriver() {
        pathToDriver = FIREFOX_DRIVER_PATH;
    }

    @Override
    public void setDriverEntryKey() {
        driverEntryKey = FIREFOX_PROPERTY_KEY;
    }

    @Override
    public void setDriver() {
        driver = new FirefoxDriver();
    }
}
