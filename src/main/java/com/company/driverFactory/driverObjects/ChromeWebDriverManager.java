package com.company.driverFactory.driverObjects;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeWebDriverManager extends WebDriverManager {

    private final String CHROME_DRIVER_PATH = "src/main/resources/drivers/chromedriver.exe";
    private final String CROME_PROPERTY_KEY = "webdriver.chrome.driver";

    //private WebDriver driver;

    public ChromeWebDriverManager() {
        setPathToDriver();
        setDriverEntryKey();
        setDriver();
    }

    @Override
    protected void setPathToDriver() {
        pathToDriver = CHROME_DRIVER_PATH;
    }

    @Override
    protected void setDriverEntryKey() {
        driverEntryKey = CROME_PROPERTY_KEY;
    }

    @Override
    public void setDriver() {
        setDriverProprety(CROME_PROPERTY_KEY, CHROME_DRIVER_PATH);
        driver = new ChromeDriver();
    }

}
