package com.company.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleHomePage extends PageObject {
    @FindBy(xpath = "//*[@id=\"tsf\"]/div[2]/div/div[1]/div/div[1]/input")
    private WebElement searchArea;

    @FindBy(xpath = "//*[@id=\"tsf\"]/div[2]/div/div[3]/center/input[1]")
    private WebElement searchBtn;

    public GoogleHomePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getSearchArea() {
        return searchArea;
    }

    public WebElement getSearchBtn() {
        return searchBtn;
    }
}
