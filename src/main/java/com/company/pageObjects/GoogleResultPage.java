package com.company.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GoogleResultPage extends PageObject {


    @FindAll(
            {@FindBy(xpath = "//*[@id=\"rso\"]/div[3]/div/div[2]/div/div/div[*]/a")}

    )
    private List<WebElement> searchResults;

    public GoogleResultPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getSearchResults() {
        return searchResults;
    }
}
