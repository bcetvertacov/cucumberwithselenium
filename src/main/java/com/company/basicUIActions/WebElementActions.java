package com.company.basicUIActions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


public class WebElementActions {


    public void fillTextBox(WebElement webElement, String text) {
        webElement.sendKeys(text);
    }

    public void clickBtn(WebElement webElement) {
        webElement.click();
    }

    public void sendKeysToTxtBox(WebElement webElement, Keys key)
    {
        webElement.sendKeys(key);
    }
}
