package com.company.utilities;

import com.google.common.base.Stopwatch;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class WaiterUtility {

    private static Random random = new Random();

    public static void waitPresenceOfElementByXpath(WebDriver driver, String xPath, long seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xPath)));
    }

    public static void waitElemntToDisapearByXPath(WebDriver driver, String ByXPath, long seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(ByXPath)));
    }

    public static void waitElemntVisibleByXPath(WebDriver driver, String ByXPath, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(ByXPath)));
    }

    public static void waitElemntToBeClickableByXPath(WebDriver driver, String ByXPath, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ByXPath)));
    }

    public static void waitElemntToBeClickable(WebDriver driver, WebElement webElement, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void TryGetElementByXpathSelector(WebDriver driver, String xPath, int seconds) {

        try {
            if (IsTimerToWaitSeconds(seconds)) {
                driver.findElement(By.xpath(xPath));
            }
        } catch (Exception e) {
            if (!IsTimerToWaitSeconds(seconds)) {
                try {
                    Thread.sleep(1000);
                    TryGetElementByXpathSelector(driver, xPath, seconds);
                } catch (InterruptedException innerExcp) {
                    innerExcp.printStackTrace();
                }
            }
            throw e;
        }
    }

    private static boolean IsTimerToWaitSeconds(int seconds) {
        Stopwatch timer = Stopwatch.createStarted();
        if (timer.elapsed(TimeUnit.SECONDS) < seconds)
            return true;
        else
            return false;
    }
}
