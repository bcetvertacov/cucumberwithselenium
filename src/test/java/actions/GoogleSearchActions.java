package actions;

import com.company.pageObjects.GoogleHomePage;
import com.company.pageObjects.GoogleResultPage;
import com.company.utilities.WaiterUtility;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import steps.BaseUITest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GoogleSearchActions extends BaseUITest {
    private BaseUITest base;

    private GoogleHomePage googleHomePage;
    private GoogleResultPage googleResultPage;

    private final String URL_TO_GET = "https://www.google.com";
    private final String TEXT_TO_SEARCH = "Moldova";

    public GoogleSearchActions(BaseUITest base) {
        this.base = base;
    }

    public void goToGoogleHomePage() {
        driver.get(URL_TO_GET);
        googleHomePage = new GoogleHomePage(driver);
    }

    public void enterTextToSearch() {
        WebElement searchArea = googleHomePage.getSearchArea();
        base.webElementActions.fillTextBox(searchArea, TEXT_TO_SEARCH);

    }

    public void clickSearchBtn() {
        WebElement searchBtn = googleHomePage.getSearchBtn();
        base.webElementActions.sendKeysToTxtBox(searchBtn, Keys.ESCAPE);
        WaiterUtility.waitElemntToBeClickable(driver, searchBtn, 10);
        base.webElementActions.clickBtn(searchBtn);
    }

    public void checkResults() {
        googleResultPage = new GoogleResultPage(driver);
        List<WebElement> listOfSearchedResluts = googleResultPage.getSearchResults();
        assertThat(listOfSearchedResluts).isNotEmpty();
    }
}
