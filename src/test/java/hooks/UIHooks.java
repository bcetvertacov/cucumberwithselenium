package hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import steps.BaseUITest;

public class UIHooks extends BaseUITest {

    protected BaseUITest baseUITestse;

    public UIHooks(BaseUITest baseUITestse) {
        this.baseUITestse = baseUITestse;
    }

    @Before
    public static void setUp() {
        BaseUITest.setUp();
    }

    @After
    public static void tearDown() {
        driver.quit();
        driver.close();
    }
}
