package steps;

import com.company.basicUIActions.WebElementActions;
import com.company.driverFactory.GetWebDriverFactory;
import com.company.driverFactory.driverObjects.WebDriverManager;
import com.company.enums.DriverTypeEnum;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class BaseUITest {
    protected static WebElementActions webElementActions;
    protected static WebDriver driver;
    protected static Actions keyActions;

    public void BaseUITest(WebElementActions webElementActions) {

    }

    public static void setUp() {
        GetWebDriverFactory driverFactory = new GetWebDriverFactory();
        WebDriverManager driverManager = driverFactory.constructDriverByType(DriverTypeEnum.CHROME);
        webElementActions = new WebElementActions();
        driver = driverManager.getWebDriver();
        keyActions = new Actions(driver);

        driver.manage().window().maximize();
    }

    public static void tearDown() {
        driver.close();
    }

}

