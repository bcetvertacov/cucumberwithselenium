package steps;

import actions.GoogleSearchActions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GoogleSteps {

    private GoogleSearchActions googleSearchActions;

    public GoogleSteps(GoogleSearchActions googleSearchActions) {
        this.googleSearchActions = googleSearchActions;
    }

    @Given("^I go to google page$")
    public void iGoToGooglePage() {
        googleSearchActions.goToGoogleHomePage();
    }

    @And("^I enter text to search$")
    public void iEnterTextToSearch() {
        googleSearchActions.enterTextToSearch();
    }

    @When("^I push search button$")
    public void iPushSearchButton() {
        googleSearchActions.clickSearchBtn();
    }

    @Then("^I see search results$")
    public void iSeeSearchResults() {
        googleSearchActions.checkResults();
    }
}
